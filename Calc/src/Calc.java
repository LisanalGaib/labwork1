import java.util.Scanner;

public class Calc {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        while (running) {
            System.out.println("Виберіть операцію:");
            System.out.println("1. Додавання");
            System.out.println("2. Віднімання");
            System.out.println("3. Множення");
            System.out.println("4. Ділення");
            System.out.println("5. Перетворення довжин");
            System.out.println("6. Розрахунок площі трикутника");
            System.out.println("0. Вихід");

            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    performAddition(scanner);
                    break;
                case 2:
                    performSubtraction(scanner);
                    break;
                case 3:
                    performMultiplication(scanner);
                    break;
                case 4:
                    performDivision(scanner);
                    break;
                case 5:
                    performLengthConversion(scanner);
                    break;
                case 6:
                    performTriangleAreaCalculation(scanner);
                    break;
                case 0:
                    running = false;
                    break;
                default:
                    System.out.println("Невірний вибір!");
            }
        }

        scanner.close();
    }
    private static void performAddition(Scanner scanner) {
        System.out.print("Введіть перше число: ");
        double num1 = scanner.nextDouble();
        System.out.print("Введіть друге число: ");
        double num2 = scanner.nextDouble();
        double result = num1 + num2;
        System.out.println("Результат: " + result);
    }
    private static void performSubtraction(Scanner scanner) {
        System.out.print("Введіть перше число: ");
        double num1 = scanner.nextDouble();
        System.out.print("Введіть друге число: ");
        double num2 = scanner.nextDouble();
        double result = num1 - num2;
        System.out.println("Результат: " + result);
    }
    private static void performMultiplication(Scanner scanner) {
        System.out.print("Введіть перше число: ");
        double num1 = scanner.nextDouble();
        System.out.print("Введіть друге число: ");
        double num2 = scanner.nextDouble();
        double result = num1 * num2;
        System.out.println("Результат: " + result);
    }
    private static void performDivision(Scanner scanner) {
        System.out.print("Введіть перше число: ");
        double num1 = scanner.nextDouble();
        System.out.print("Введіть друге число: ");
        double num2 = scanner.nextDouble();
        if (num2 == 0) {
            System.out.println("Ділення на нуль неможливе!");
        } else {
            double result = num1 / num2;
            System.out.println("Результат: " + result);
        }
    }
    private static void performTriangleAreaCalculation(Scanner scanner) {
        System.out.print("Введіть основу трикутника: ");
        double base = scanner.nextDouble();
        System.out.print("Введіть висоту трикутника: ");
        double height = scanner.nextDouble();

        double area = 0.5 * base * height;
        System.out.println("Площа трикутника: " + area);
    }
    private static void performLengthConversion(Scanner scanner) {
        System.out.println("Виберіть одиницю вимірювання:");
        System.out.println("1. Метри");
        System.out.println("2. Сантиметри");
        System.out.println("3. Міліметри");
        int choice = scanner.nextInt();

        System.out.print("Введіть значення: ");
        double value = scanner.nextDouble();

        double meters, centimeters, millimeters;
        switch (choice) {
            case 1:
                meters = value;
                centimeters = value * 100;
                millimeters = value * 1000;
                System.out.println(value + " метрів = " + centimeters + " сантиметрів = " + millimeters + " міліметрів");
                break;
            case 2:
                centimeters = value;
                meters = value / 100;
                millimeters = value * 10;
                System.out.println(value + " сантиметрів = " + meters + " метрів = " + millimeters + " міліметрів");
                break;
            case 3:
                millimeters = value;
                meters = value / 1000;
                centimeters = value / 10;
                System.out.println(value + " міліметрів = " + meters + " метрів = " + centimeters + " сантиметрів");
                break;
            default:
                System.out.println("Невірний вибір!");
        }
    }
}